<?php
require "vendor/autoload.php";


$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::before('start', function(&$params, &$output){
    ORM::configure('sqlite:caracteristiques.sqlite3');
});

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addGlobal('ma_valeur', "Hello There!");
});

//--------------------------------------

Flight::route('/', function(){
    $data = [
        'telephones' => Model::factory('Smartphone')->find_many(),
    ];
    Flight::view()->display('tel.twig', $data);
});

Flight::route('/Contacts', function(){
    $data = [
        'dinosaures' =>  getDinosaures(),
    ];
    Flight::view()->display('contacts.twig', $data);
});


//--------------------------------------

Flight::route('/details/@name', function($name){
    $data = [
        /*'details' =>  getDinosauresDetail($name),*/

        'details' => Model::factory('Smartphone')->where('nom', $name)->find_one(),
    ];
    Flight::view()->display('tel_detail.twig', $data);
});


Flight::start();
//--------------------------------------




?>