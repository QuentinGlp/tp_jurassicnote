<?php
require "vendor/autoload.php";

ORM::configure('sqlite:ingen.sqlite3');

function importCaracteristiqueFromCSVFile($filepath)
{
    $csv_file = fopen($filepath,'r');
    ini_set('auto_detect_line_endings',TRUE);
    while ( ($data = fgetcsv($csv_file) ) !== FALSE ) {
        $telephone = Model::factory('Smartphone')->create();

        $telephone->Marque = $data[0];
        $telephone->Nom = $data[1];
        $telephone->Prix = $data[2];
        $telephone->Taille = $data[3];
        $telephone->Definition = $data[4];
        $telephone->Memoire = $data[5];
        $telephone->MicroSD = $data[6];
        $telephone->Processeur = $data[7];
        $telephone->Frequence = $data[8];
        $telephone->RAM = $data[9];
        $telephone->GPU = $data[10];
        $telephone->Autonomie = $data[11];
        $telephone->QuickCharge = $data[12];
        $telephone->SIM = $data[13];
        $telephone->USB = $data[14];
        $telephone->OS = $data[15];
        $telephone->Hauteur = $data[16];
        $telephone->Largeur = $data[17];
        $telephone->Epaisseur = $data[18];
        $telephone->Poids = $data[19];
        $telephone->Index = $data[20];

        $telephone->save();
    }
    ini_set('auto_detect_line_endings',FALSE);
}

importCaracteristiqueFromCSVFile("caracteristiques.csv");